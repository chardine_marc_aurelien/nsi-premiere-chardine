# Bienvenue sur notre site

Ceci est la page d'accueil de notre site.
Vous pouvez y ajouter du contenu, des images, des liens, etc.

```python linenums='1'
a = 1
b = 2
t = b
b = a
a = t
```

```python 
note = 15
if note >= 16:  # (1)
    print("TB")
elif note >= 14:  # (2)
    print("B")
elif note >= 12:  # (3)
    print("AB")
elif note >= 10:
    print("reçu")
else:
    print("refusé")
```

1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

2. :warning: `elif` signifie **sinon si**.

3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

!!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"



```python hl_lines="3 5-6"
def maximum_tab(tableau):
    assert tableau != [], "le tableau est vide"
    maximum = tableau[0]
    for valeur in tableau:
        if valeur > maximum:
            maximum = valeur
    return maximum
```

{{ IDE('scripts/script1.py', MAX=3, SANS='sum') }}